import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class AutomationPracticeRegistration {

	public static void main(String[] args) {
		
		// Set driver path
		System.setProperty("webdriver.chrome.driver", "e:\\drivers\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("http:\\automationpractice.com");
//		By locator = By.className("login");
//		WebElement element = driver.findElement(locator);
//		element.click();
		driver.findElement(By.className("login")).click();
		driver.findElement(By.id("email_create")).sendKeys("vikasthange@gmail.com");
		driver.findElement(By.id("SubmitCreate")).click();
		// TODO: wait for next page to load
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String pageHeading = driver.findElement(By.className("page-heading")).getText();
		System.out.println("Page Heading: "+pageHeading);
		
		WebElement eleState = driver.findElement(By.name("id_state"));
		Select selectState = new Select(eleState);
		selectState.selectByVisibleText("Georgia");
	}
}
