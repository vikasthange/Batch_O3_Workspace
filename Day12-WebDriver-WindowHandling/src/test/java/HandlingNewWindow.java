import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class HandlingNewWindow {

	@Test
	public void testCaseSwitchingToFrame(){
		System.setProperty("webdriver.chrome.driver", "e:\\drivers\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("file:///C:/Users/vikas/Desktop/Day12-openlinkinnewtab.html");
		
		System.out.println("Window handle of current window: "+ driver.getWindowHandle());
		Set<String> allHandles = driver.getWindowHandles();
		System.out.println("How many windows before click? :"+ allHandles.size());
		driver.findElement(By.linkText("Vikas Thange Blog")).click();
		System.out.println("Window handle After Click: "+ driver.getWindowHandle()); // same handle
		allHandles = driver.getWindowHandles();
		System.out.println("How many windows after click? :"+ allHandles.size());
		System.out.println("Browser Title: "+ driver.getTitle());
	}
}
