import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

public class Example1 {

	public static void main(String[] args) {
		
		HashMap<Integer, String> studentData = new HashMap<Integer, String>();
		
		// 101 - Vikas Thange
		// 102 - ABCD
		studentData.put(101, "Vikas Thange");
		studentData.put(102,"Prajakta Gawade");
		studentData.put(103, "Farhan Patel");
		
		System.out.println("102 roll no - "+ studentData.get(102));
		
		Set<Integer> allKeys = studentData.keySet();
		for (Integer key : allKeys) {
			System.out.println(studentData.get(key));
		}
		
		Set<Entry<Integer, String>> entries = studentData.entrySet();
		
		Iterator<Entry<Integer, String>> itr = entries.iterator();
		while(itr.hasNext()){
			Entry<Integer, String> entry = itr.next();
			System.out.println(entry.getKey()+" -- "+ entry.getValue());
		}
		
	}
}
