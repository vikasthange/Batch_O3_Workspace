import java.util.List;
import java.util.Set;
import java.util.TreeMap;

public class Example2 {

	public static void main(String[] args) {
		
		TreeMap<String, String> map = new TreeMap<String,String>();
		map.put("Mumbai", "Maharashtra");
		map.put("Bangaluru", "Karnataka");
		
		System.out.println( map.get("Bangaluru"));
		
		Set<String> allKeys = map.keySet();
		System.out.println("Keys: "+ allKeys);
		
		for (String key : allKeys) {
			System.out.println("City: "+key+", State: "+map.get(key));
		}
		
		
	}
}
