import java.util.Scanner;

public class Example3 {

	public static void main(String[] args) {
		
		System.out.print("Enter your name: ");
		
		Scanner scanner = new Scanner(System.in);
		
		String name = scanner.nextLine();
		System.out.println("Hello Mr. "+ name);
		
		System.out.println("Please enter your age: ");
		int age = scanner.nextInt();
		if(age<18){
			System.out.println("Not Allowed to...");
		}
		scanner.close();
		
	}
}
