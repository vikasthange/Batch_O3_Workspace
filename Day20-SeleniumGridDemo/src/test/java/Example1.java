import java.net.URL;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.Test;

public class Example1 {

	@Test
	public void testCase1() {

		//System.setProperty("webdriver.chrome.driver", "e:\\drivers\\chromedriver.exe");
		// WebDriver driver = new ChromeDriver(); // without selenium GRID
		try {
			URL url = new URL("http://192.168.0.106:4444/wd/hub");

			DesiredCapabilities caps = new DesiredCapabilities();
			caps.setBrowserName("chrome");

			WebDriver driver = new RemoteWebDriver(url, caps);
			driver.get("http://automationpractice.com");
			System.out.println(driver.getTitle());
			Thread.sleep(30000);
			driver.quit(); // close session (closes all browsers)
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	@Test
	public void testCase2(){
		testCase1();
	}
}
