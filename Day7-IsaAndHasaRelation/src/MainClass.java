
public class MainClass {

	public static void main(String[] args) {
		
//		Mango m = new Mango();
//		
//		m.makeMilkshake();
		
//		Fruit f = new Fruit();
//		f.makeMilkshake();

		Fruit f = new Mango();
	
		f.makeMilkshake(); // call to mango class method.
		
		Mango m = (Mango)f;
		int count = m.getTypesOfMangoesCount();
		System.out.println(count);
		
		//ParentClass o = new ChildClass();
		// you can only call ParentClass methods on object o
		
		
//	
//		
		/*
		1. Error we can not store Mango object in Fruit variable
		2. No Error - yummy mango shake!! (Right Answer)
		3. No Error - It's fruit shake
		
		*/
	}
}
