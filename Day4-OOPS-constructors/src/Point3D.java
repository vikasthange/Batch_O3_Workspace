
public class Point3D {

	int x,y,z; // scope: Class member / field
	
	public Point3D(int x, int y, int z){ // Scope: Local
		this.x = x; // local
		this.y = y;
		this.z = z;
	}
	public void print(){
		System.out.println(x+","+y+","+z);
	}
	public static void main(String[] args) {
		Point3D p = new Point3D(10, 20, 30);
		p.print(); // 0,0,0
	}
}
