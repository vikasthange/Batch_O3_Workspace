
public class Point2D {

	// Data
	private int x, y;

	public Point2D(int i) { // one param
		x = y = i;
	}

	public Point2D() { // default

	}

	public Point2D(int i, int j) {
		x = i;
		y = j;
	}

	// Methods
	public void print() {
		System.out.println("X:" + x + ", Y: " + y);
	}

	public void setPoint(int a, int b) {
		x = a;
		y = b;
	}
}
