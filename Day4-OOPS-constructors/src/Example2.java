
public class Example2 {

	public static void main(String[] args) {
		
		Point2D p1 = new Point2D();  // default 
		Point2D p2 = new Point2D(10); // one 
		Point2D p3 = new Point2D(10,20); // 2 params
		
		p1.print(); // before setting value it will print defaults 0 0
		p1.setPoint(20, 50);
		p1.print(); // 20,50
		
		p2.print();
		
		
	}
	
	/*
	 * If  class is not having any constructor then JVM add a default constructor
	 * 
	 * but if you define at least one constructor to class then JVM is not going 
	 * to add default constructor
	 * 
	 * 
	 */
}
