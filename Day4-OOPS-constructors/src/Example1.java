
public class Example1 {

	
	public static void main(String[] args) {
		Student stud1 = new Student("Vikram",101,"Balgalore","21-Jan-1987","ABC School"); // calling to constructor
		Student stud2 = new Student("PQR",102,"Delhi","12-May-1973","Don B school"); // constraint on object creation
		Student stud3 = new Student("Anshool",104,"Mumbai","4-Jul-1987","SCH Name");
		
		Student student = new Student(); // default 
		student.setData("NAME", 105, "TN,IND", "Date", "Sch name");
		
//		stud1.printStudentDetails();
//		stud2.printStudentDetails();
//		stud3.printStudentDetails();
		student.printStudentDetails();
		
	}
}

/*
	Constructor is a method
	(Special)
	Class name and constructor name must be same
	Constructors do not have return type - not even void
	
	
	Types of constructors
	1. Default (no input parameters / arguments)
	2. Parameterized constructor (it takes input parameter)
	
	Why constructor:
	1. TO initialize object (or set default as well)
	2. To Have constraint on object creation
	
	OVERLOADING
	- Having multiple methods with same method names but different parameters
	- Constructor Overloading
	
	How many overloaded constructors?
	-> no limit as long as the parameters are different
	
	1. No parameters
	2. The datatypes
	3. The sequence of data types
	
*/