import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class NewToursLoginTest {

	public static void main(String[] args) {
		
		System.setProperty("webdriver.chrome.driver", "e:\\drivers\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("http://newtours.demoaut.com");
		
		//driver.findElement(By.name("userName")).sendKeys("tutorial");
		driver.findElement(By.cssSelector("[name='userName']")).sendKeys("tutorial");
		driver.findElement(By.cssSelector("[name='password']")).sendKeys("tutorial");
		driver.findElement(By.cssSelector("[type='image']")).click();
		
		System.out.println("After login: "+ driver.getCurrentUrl());
		
	}
}
