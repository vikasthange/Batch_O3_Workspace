package organic;

public class Mango extends Fruit{

	Mango(){
		super();
		this.color= "yellow";
		this.t= "sweet";
	}
	@Override
	public void makeMilkshake(){ // this will be overriding the method coing from base class
		// same method name
		// same parameters
		System.out.println("yummy mango shake!!");
		System.out.println("Calling method from base class");
		super.makeMilkshake(); // fruit shake
		
	}
	
}
