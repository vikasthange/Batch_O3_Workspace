import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class NastedFramesExample {

	@Test
	public void testCase(){
		System.setProperty("webdriver.chrome.driver", "e:\\drivers\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("file:///C:/Users/vikas/Desktop/BatchO3Frames.html");
		System.out.println(driver.getTitle());
		driver.switchTo().frame( driver.findElement(By.tagName("iframe")) );
		System.out.println(driver.getTitle());
		driver.switchTo().frame("double_click_ex");
		System.out.println(driver.getTitle());
		
		String actualText = driver.findElement(By.cssSelector("p[ondblclick]")).getText();
		System.out.println(actualText);
		
		//driver.switchTo().parentFrame(); // will go to the frame Alerts frame
		
		driver.switchTo().defaultContent(); // will switch to main webpage
		
	}
}
