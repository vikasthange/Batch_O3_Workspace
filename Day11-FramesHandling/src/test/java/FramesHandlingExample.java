import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class FramesHandlingExample {

	@Test
	public void testCaseSwitchingToFrame(){
		System.setProperty("webdriver.chrome.driver", "e:\\drivers\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("file:///C:/Users/vikas/Desktop/BatchO3Frames.html");
		
		String textFromMainPage = driver.findElement(By.tagName("p")).getText();
		Assert.assertEquals(textFromMainPage, "This is main web page","Text from main page failed");
		// 3 ways to switch frame
//		//1. Use WebElement
		WebElement frameElement = driver.findElement(By.tagName("iframe"));
		driver.switchTo().frame(frameElement);
		
		// 2. Switch by name or id
//		driver.switchTo().frame("name or id of your frame");
		
		//3. Switch by frame index number 
		System.out.println("------ HTML Before Switching frame -------");
		Reporter.log(driver.getPageSource(),true);
		driver.switchTo().frame(0);
		System.out.println("------ HTML After Switching frame --------");
		Reporter.log(driver.getPageSource(),true);
		
		driver.findElements(By.tagName("input")).get(0).click();
	}
	
}
