
public class Example2 {

	public static void main(String[] args) {
		Student stud = new Student();
		// Above line allocates memory
		// also the variables will get default value
		stud.printStudentDetails();
		//How to set value?
		//stud.name="Vikas"; // not recommanded
		System.out.println("Setting data");
		stud.setData("Vikas", 101, "Pune", "21-8-91", "XYZ School");
		stud.printStudentDetails();
	}
}
