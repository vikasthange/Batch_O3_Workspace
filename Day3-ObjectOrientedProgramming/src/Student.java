
public class Student {

	// Data
	// Keep data private 
	private String name; // null
	private int rollNo;  // 0
	private String addr; // null
	private String dob; // null
	private String schoolName; // Camel casing	
	// Methods - As public
	public void appearExam(){
		System.out.println("Appeared for exam");
	}
	public void enrollCourse(){
		System.out.println("Enrolled Course");
	}
	public void doStudy(){
		System.out.println("doing study");
	}
	public int getAge(){
		return 30;
	}
	public void printStudentDetails(){
		System.out.println("Name: "+name);
		System.out.println("Roll No: "+ rollNo);
		System.out.println("Address: "+addr);
		System.out.println("Date of birth : "+dob);
		System.out.println("Age: "+ getAge());
		System.out.println("School name: "+schoolName);
	}
	
	public void setData(String n,int rno,String a,String d,String schName){
		name = n;
		rollNo = rno;
		addr = a;
		dob = d;
		schoolName = schName;
	}
}
