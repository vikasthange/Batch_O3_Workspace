import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.Test;

import com.google.common.io.Files;

public class SampleCloudTestCase {

	@Test
	public void testCaseGoogleSearch() {

		// Cloud -> giant Selenium Grid

		final String USERNAME = "vikast1";
		final String AUTOMATE_KEY = "qzyZts6XhMVLBj5TozVt";
		final String urlString = "https://" + USERNAME + ":" + AUTOMATE_KEY + "@hub-cloud.browserstack.com/wd/hub";
		DesiredCapabilities caps = new DesiredCapabilities();

		caps.setCapability("os", "OS X");
		caps.setCapability("os_version", "High Sierra");
		caps.setCapability("browser", "Safari");
		caps.setCapability("browser_version", "11.0");
		
		caps.setCapability("browserstack.local", "false");
		caps.setCapability("browserstack.debug", "true");
		caps.setCapability("browserstack.selenium_version", "3.5.2");
		try {
			URL url = new URL(urlString);
			
			WebDriver driver = new RemoteWebDriver(url,caps);
			driver.get("http://google.com");
			driver.findElement(By.name("q")).sendKeys("Vikas Thange"+Keys.ENTER);
			
			TakesScreenshot takeSnap = (TakesScreenshot) driver;
//			File snap = takeSnap.getScreenshotAs(OutputType.FILE);
//			Files.move(snap, new File("snap.png"));
//			System.out.println("Test execution done. Snap captured at: "+new File("snap.png").getAbsolutePath());
			driver.quit();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}; // 

	}
}
