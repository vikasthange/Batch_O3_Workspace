import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class Example1 {

	@Test
	public void testNewWindow(){
		
		System.setProperty("webdriver.chrome.driver", "e:\\drivers\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("file:///C:/Users/vikas/Desktop/Day12%20sample.HTML");
		driver.findElement(By.linkText("Visit W3Schools.com!")).click();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		Set<String> allHandles = driver.getWindowHandles();
		System.out.println("Presently there are : "+ allHandles.size()+" windows/tabs opened");
		// switch to  new window
		// 1 -> my html web page (first Page / windows)
		// 2 -> to w3schools page
		
//		driver.switchTo().window(allHandles.get(1));
		for (String handle : allHandles) {
			System.out.println("switching to -> : "+ handle);
			driver.switchTo().window(handle);
		}
		System.out.println("Current Page Url: "+ driver.getCurrentUrl());
	}
}
