import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class Example2 {

	@Test
	public void testNewWindow(){
		
		System.setProperty("webdriver.chrome.driver", "e:\\drivers\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		// tab 1
		driver.get("https://news.google.com/news/?ned=in&gl=IN&hl=en-IN");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.MINUTES);
		//open tab 2
		driver.findElement(By.linkText("PNB closed all options to recover dues by going public: Nirav Modi in letter to bank")).click();
		// open tab 3
		try {
			Thread.sleep(2000);
			driver.findElement(By.linkText("A fraud perpetrated right under the noses of PNB bosses")).click();
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		Set<String> allHandles = driver.getWindowHandles();
		System.out.println("Presently there are : "+ allHandles.size()+" windows/tabs opened");
		for (String handle : allHandles) {
			System.out.println("switching to -> : "+ handle);
			
			driver.switchTo().window(handle);
//			if(driver.getCurrentUrl().contains("thehindu")){
//				break;
//			}
		}
		System.out.println("Current Page Url: "+ driver.getCurrentUrl());
	}
}
