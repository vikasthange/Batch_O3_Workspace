package users;


public class MainClass {

	public static void main(String[] args) {
		
		Employee e = new Employee(101, "Vikas T", "IT-Software", "Pune", 10000);
		
		//e.getSalary(); // // Private members are not accessible out side the same class.
		e.getDept();
		String loc = e.getLocation(); // defaults are accessible in same package
	}
}
