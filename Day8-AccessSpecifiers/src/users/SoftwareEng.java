package users;

public class SoftwareEng extends Employee{
	String techno ;
	SoftwareEng(int empId, String name, String dept, String location, double salary, String techno) {
		super(empId, name, dept, location, salary);
		this.techno = techno;
	}

	public void printDept(){
		System.out.println(this.getDept());
		// super.getDept() and this.getDept() both are same
		
	}
	
	
}
