package users;

public class Employee {

	private int empId;
	private String name;
	private String dept;
	private String location;
	double salary;
	public Employee(int empId,String name,String dept, String location,double salary){
		this.empId = empId;
		this.name= name;
		this.dept=dept;
		this.location=location;
		this.salary = salary;
	}
	
	private double getSalary(){
		return this.salary;
	}
	protected String getDept(){
		return dept;
	}
	String getLocation(){
		return location;
	}
}
