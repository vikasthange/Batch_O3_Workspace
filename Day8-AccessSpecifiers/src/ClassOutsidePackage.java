import users.Employee;

public class ClassOutsidePackage{

	public static void main(String[] args) {
		
		Employee e = new Employee(101, "Vikas T", "IT-Software", "Pune", 10000);
		
		//e.getDept(); // not allowed
	}
}
