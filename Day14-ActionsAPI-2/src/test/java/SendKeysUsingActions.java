import java.awt.Desktop.Action;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

public class SendKeysUsingActions {
	@Test
	public void testSubnavMenuMen() throws InterruptedException{
		
		System.setProperty("webdriver.chrome.driver", "e:\\drivers\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://www.w3.org/2002/09/tests/keys-cancel2.html");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		WebElement txt = driver.findElement(By.id("entry"));
		Thread.sleep(5000);
		Actions a = new Actions(driver);
		a.click(txt).keyDown(Keys.SHIFT).sendKeys("vikas").keyUp(Keys.SHIFT).perform();
	}
}
