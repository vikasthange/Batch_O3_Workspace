import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.Test;

public class MouseOverExample {

	@Test
	public void testSubnavMenuMen(){
		
		System.setProperty("webdriver.chrome.driver", "e:\\drivers\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("http://flipkart.com");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		// otional wait/ wait will only occur if locator was not matching to any element
		driver.findElement(By.xpath("//*[text()='✕']")).click();
		WebElement mainNavMenuMen = driver.findElement(By.cssSelector("[title='Men']"));
		
		Actions action = new Actions(driver);
		action.moveToElement(mainNavMenuMen).click().perform();// mouse will move to center of element
		
		try {
			Thread.sleep(5000); // mandatory wait
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		WebElement subNavMenu = driver.findElement(By.cssSelector("[title='Footwear']"));
		Assert.assertTrue(subNavMenu.isDisplayed(),"Sub nav was not opened after hovering mouse on Menu 'Men'");
		
	}
}
