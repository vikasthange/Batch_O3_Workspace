import java.awt.Desktop.Action;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.Test;

public class RightClickMenu {

	@Test
	public void testSubnavMenuMen(){
		
		System.setProperty("webdriver.chrome.driver", "e:\\drivers\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://swisnl.github.io/jQuery-contextMenu/demo.html");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		WebElement btn = driver.findElement(By.className("context-menu-one"));
		Actions a = new Actions(driver);
		a.contextClick(btn).perform();
		
		driver.findElement(By.className("context-menu-icon-edit")).click();
		Alert alert = driver.switchTo().alert();
		String actualAlertText = alert.getText();
		Assert.assertEquals(actualAlertText, "clicked: edit","Alert text failed");
		
		// a.moveToElement(ele).contextClick().perform()
	}
}
