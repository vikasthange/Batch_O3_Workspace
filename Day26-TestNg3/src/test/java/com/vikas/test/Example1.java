package com.vikas.test;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

public class Example1 {

	WebDriver driver;
	@BeforeSuite
	public void initBrowser(){
		System.setProperty("webdriver.chrome.driver", "e:\\drivers\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("http://demo.testfire.net");
	}
	@AfterSuite
	public void afterSuite(){
		
		driver.quit();
	}
	@BeforeMethod
	public void openLoinPage(){
		driver.findElement(By.linkText("ONLINE BANKING LOGIN")).click();
	}
	@AfterMethod
	public void logOut(){
		
		driver.findElement(By.linkText("Sign Off")).click();
	}
	@Test(groups={"G1","G2"})
	public void testLogin(){
		System.out.println("Test 1");
		driver.findElement(By.id("uid")).clear();
		driver.findElement(By.id("uid")).sendKeys("jsmith");
		driver.findElement(By.id("passw")).sendKeys("demo1234");
		driver.findElement(By.id("passw")).submit();
		//driver.findElement(By.name("btnSubmit")).click();
		String welcomeMessage = driver.findElement(By.tagName("h1")).getText();
		Assert.assertEquals(welcomeMessage, "Hello John Smith","Greeting message failed");
		
	}
	
	@Test(groups="G2")
	public void testAdminLogin(){
		System.out.println("Test 2");
		driver.findElement(By.id("uid")).clear();
		driver.findElement(By.id("uid")).sendKeys("admin");
		driver.findElement(By.id("passw")).sendKeys("admin");
		driver.findElement(By.id("passw")).submit();
		//driver.findElement(By.name("btnSubmit")).click();
		
		String welcomeMessage = driver.findElement(By.tagName("h1")).getText();
		Assert.assertEquals(welcomeMessage, "Hello Admin User","Greeting message failed");
		
	}
}
