public class Person {

	String name;
	String dob;
//	Person(){ // default constructor
//		System.out.println("in person class constructor");
//	}
	Person(String name, String dob){
		this.name=name;
		this.dob=dob;
	}
	public static void main(String[] args) {
		Student s = new Student("Vikas","21-A-2893",101,"ABC");
		
		
	}
}

class Student extends Person{
	
	int rollNo;
	String schoolName;
	public Student(String name, String dob,int rollNo, String schoolName) {
		//super();  //JVM Adds call to bas class constructor
		super(name,dob);
		this.rollNo=rollNo;
		this.schoolName=schoolName;
		System.out.println("Student class constructor");
	}
}
