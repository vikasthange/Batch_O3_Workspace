
public class PrimeCheck {

	public static void main(String[] args) {
		
		int n = 71;
		
		boolean retValue = checkNumberIsPrime(n);
		System.out.println("Number : "+n+" is prime? "+ retValue);
	}

	private static boolean checkNumberIsPrime(int no) {
		
		boolean isPrime = true;
		for (int i = 2; i < no; i++) {
			
			if( no%i == 0){
				isPrime= false;
				break;
			}
		}
		//System.out.println("Is "+no +"prime: "+ isPrime );
		return isPrime;
	}

}
