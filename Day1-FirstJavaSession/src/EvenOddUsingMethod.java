
public class EvenOddUsingMethod {

	public static void main(String[] args) {
		//checkEvenOdd(27);
		sayHi();
	}
	
	public static void checkEvenOdd(int no){
		
		int rem = no % 2;
		if(rem == 0){
			System.out.println("Number: " + no + " is Even");
		}
		else {
			System.out.println("Number : "+ no +" is Odd");
		}
	}
	public static void sayHi(){
		System.out.println("Hi there...");
	}
}
