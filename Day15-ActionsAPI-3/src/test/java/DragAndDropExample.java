import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

public class DragAndDropExample {
	@Test
	public void testDragAndDrop() throws InterruptedException{
		
		System.setProperty("webdriver.chrome.driver", "e:\\drivers\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://dhtmlx.com/docs/products/dhtmlxTree/");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		String sourceLocator="//*[@id='treebox1']//*[text()='Books']";
		String targetLocator = "//*[@id='treebox2']//*[text()='Magazines']";
		
		WebElement source = driver.findElement(By.xpath(sourceLocator));
		WebElement target = driver.findElement(By.xpath(targetLocator));
		
		Actions a = new Actions(driver);
		a.dragAndDrop(source, target).perform();
//		a.dragAndDropBy(source, -100, 20);
	}
}
