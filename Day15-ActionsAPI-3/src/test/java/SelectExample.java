import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SelectExample {
	@Test
	public void testDragAndDrop() throws InterruptedException{
		
		System.setProperty("webdriver.chrome.driver", "e:\\drivers\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("file:///C:/Users/vikas/Desktop/dopdown%20list.html");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		WebElement selectElement = driver.findElement(By.tagName("select"));
		
		Select selectCompany= new Select(selectElement);
		//3 ways you can select a value
		//System.out.println( "Presently select: "+ selectCompany.getFirstSelectedOption().getText() );
		// 1 useing visible text
		//selectCompany.selectByVisibleText("Opel");
		// select by index position
		//selectCompany.selectByIndex(3);
		
		// select element by it's value
		selectCompany.selectByValue("saab");
		selectCompany.selectByVisibleText("Volvo");
		//Assert.assertEquals(selectCompany.getFirstSelectedOption().getText(),"Saab","Value failed" );
		selectCompany.deselectByVisibleText("Audi");
		List<WebElement> allSelected = selectCompany.getAllSelectedOptions();
		for (int i = 0; i < allSelected.size(); i++) {
			System.out.println(allSelected.get(i).getText());
		}
		
		
		
	}
}
