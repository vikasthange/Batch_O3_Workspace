package com.vikas.example2;

public class MainClass {

	public static void main(String[] args) {
		
		ComplexEquation e1 = new ComplexEquation(2, 3);
		ComplexEquation e2 = new ComplexEquation(43, 9);
		
		ComplexEquation e3 = e1.add(e1); 
		e3.print();
		
		ComplexEquation e4 = ComplexEquation.add(e1, e2);
		System.out.println("Values of E4 is ");
		e4.print();
		
		
		//e3 = e1 + e2 
		//Java do not have operator overloading
		
	}
}
