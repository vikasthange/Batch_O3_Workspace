package com.vikas.example2;

public class ComplexEquation {

	double a;
	final double IMAGINARY=1.0;
	double b;
	ComplexEquation(double a, double b){
		this.a = a;
		this.b = b;
	}
	
	public void print(){
		System.out.println("Complex eq: "+this.a + "+"+this.b+"i");
	}

	public ComplexEquation add(ComplexEquation e) {
		ComplexEquation tmp; // decleration
		double a = this.a + e.a;
		double b = this.b + e.b;
		
		tmp = new ComplexEquation(a, b);
		return tmp;
	}
	
	public static ComplexEquation add(ComplexEquation e1, ComplexEquation e2){
		return new ComplexEquation(e1.a + e2.a, e1.b + e2.b);	
	}
	
	
}
