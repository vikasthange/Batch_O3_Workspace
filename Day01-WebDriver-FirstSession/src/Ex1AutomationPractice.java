import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ByIdOrName;

public class Ex1AutomationPractice {

	public static void main(String[] args) {
		
		// Write AUtomation script
		// My project needs webdriver
		
		// Components of Selenium
		/*
		 * 1. Selenium IDE
		 * 			- Record playback
		 * 			- Export / convert test cases into Programming
		 * 			- Firefox Addon (GUI)
		 * 			- No one uses it for real project 
		 * 			- Out dated, No longer supported from Firefox 55 onwards
		 * 			- JavaScript  (JS DOM) document.getElementById("uname").value="vikas"
		 * 2. Selenium Remote Control RC
		 * 			- Considered as Selenium 1.0
		 * 			- Having programming lang support
		 * 			- Proxy Server which injects JS Library in your WebPage for automation
		 * 			- Con - No Object Oriented programming , No Same Origin Policy
		 * 3. WebDriver
		 * 			- 2.0   (2.53.1)
		 * 			- 3.0   ( 3.X)
		 * 			- Object Oriented programming
		 * 			- Multiple lang - Java, C# , Python, JavaScript, Ruby, PHP, ....
		 * 4. Selenium GRID
		 * 			- Parallel and Distributed test execution
		 * 
		 */
		
		System.setProperty("webdriver.chrome.driver", "e:/drivers/chromedriver.exe");
		WebDriver driver = new ChromeDriver(); // launch Chrome browser
		driver.get("http://automationpractice.com");
		By locator = new ByIdOrName("search_query_top");
		WebElement searchBox = driver.findElement(locator);
		searchBox.sendKeys("Dress"); // type 
	}
}
