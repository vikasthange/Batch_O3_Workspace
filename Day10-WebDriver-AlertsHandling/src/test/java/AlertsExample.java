import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

public class AlertsExample {

	
	@Test
	public void testAlert(){
		System.setProperty("webdriver.chrome.driver", "e:\\drivers\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("file:///C:/Users/vikas/Desktop/BatchO3.html");
		
		driver.findElements(By.tagName("input")).get(0).click();
		
		Alert alert = driver.switchTo().alert();
		// What to do?
		//waitForSeconds(10);
		String alertMesage = alert.getText();
		Assert.assertEquals(alertMesage, "Its nice day today..","Alert text failed");
		alert.accept();
		
		System.out.println(driver.getPageSource()); 
		// will throw UnhandledAlertException: unexpected alert open: {Alert text : Its nice day today..}
		driver.quit();
	}
	private void waitForSeconds(int sec){
		try {
			Thread.sleep(sec *1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@Test
	public void testConfirmWithYesOption(){
		System.setProperty("webdriver.chrome.driver", "e:\\drivers\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("file:///C:/Users/vikas/Desktop/BatchO3.html");
		
		driver.findElements(By.tagName("input")).get(1).click();
		
		Alert alert = driver.switchTo().alert();
		// What to do?
		//waitForSeconds(10);
		alert.accept();
		
		String actualText = driver.findElement(By.id("ex2Result")).getText();
		Assert.assertEquals(actualText, "Let's Continue to next section...","Message on accepting alert failed");
		driver.quit();
	}
	@Test
	public void testConfirmWithCancelOption(){
		System.setProperty("webdriver.chrome.driver", "e:\\drivers\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("file:///C:/Users/vikas/Desktop/BatchO3.html");
		
		driver.findElements(By.tagName("input")).get(1).click();
		
		Alert alert = driver.switchTo().alert();
		// What to do?
		//waitForSeconds(10);
		alert.dismiss();
		
		String actualText = driver.findElement(By.id("ex2Result")).getText();
		Assert.assertEquals(actualText, "ohh.. Lets revise the same topic again..","Message on dismissing alert failed");
		driver.quit();
	}
	@Test
	public void testPromptWithUserInput(){
		System.setProperty("webdriver.chrome.driver", "e:\\drivers\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("file:///C:/Users/vikas/Desktop/BatchO3.html");
		
		driver.findElements(By.tagName("input")).get(2).click();
		
		Alert alert = driver.switchTo().alert();
		// What to do?
		alert.sendKeys("Vikas Thange");
		//waitForSeconds(10);
		alert.accept(); // click on ok
		
		String actualText = driver.findElement(By.id("ex3Result")).getText();
		Assert.assertEquals(actualText, "Vikas Thange","Message on prompt with valid input failed");
		driver.quit();
	}
	@Test
	public void testPromptWithEmptyUserInput(){
		System.setProperty("webdriver.chrome.driver", "e:\\drivers\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("file:///C:/Users/vikas/Desktop/BatchO3.html");
		
		driver.findElements(By.tagName("input")).get(2).click();
		
		Alert alert = driver.switchTo().alert();
		// What to do?
//		alert.sendKeys("Vik");
		//waitForSeconds(10);
		alert.accept(); // click on ok
		
		String actualText = driver.findElement(By.id("ex3Result")).getText();
		Assert.assertEquals(actualText.trim(), "","Message on prompt with empty input failed");
		driver.quit();
	}
	@Test
	public void testPromptWithCancelButton(){
		System.setProperty("webdriver.chrome.driver", "e:\\drivers\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("file:///C:/Users/vikas/Desktop/BatchO3.html");
		
		driver.findElements(By.tagName("input")).get(2).click();
		
		Alert alert = driver.switchTo().alert();
		// What to do?
//		alert.sendKeys("Vik");
		//waitForSeconds(10);
		alert.dismiss(); // click on ok
		
		String actualText = driver.findElement(By.id("ex3Result")).getText();
		Assert.assertEquals(actualText.trim(), "null","Message on prompt with cancel option failed");
		driver.quit();
	}
	
}
