
public class MainClassExample1 {

	static{
		System.out.println("this is static block");
		
		// constructor is used to initilize object
		// where as static block is mostly used to initilize static vatiables
	}
	public static void main(String[] args) {
		System.out.println("Main method started");
		Point2D p1 = new Point2D(10,20);
		Point2D p2 = new Point2D(15,32);
		
		System.out.print("P1 : ");
		p1.print();
		System.out.print("P2 : ");
		p2.print();
		
		Point2D.sayHi(); // static method
		// static methods are always called in class name
		// we do not need any object to call static methods
	
		
	}
}
