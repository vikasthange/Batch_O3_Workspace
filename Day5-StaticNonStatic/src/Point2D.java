
public class Point2D {

	int x, y;
	static float PI=3.14f;
	static{
		System.out.println("Static block of Point2D");
	}
	public Point2D(){
		
	}
	public Point2D(int x, int y){
		// two x variables
		this.x = x;
		this.y = y;
	}
	public void print(){
//		x and this.x both are same
		System.out.println("x: "+ this.x +", y: "+ this.y );
		sayHi();
	}
	public static void sayHi(){
		System.out.println("Hi there!....");
		System.out.println("Value of PI: "+PI);
		// you can not use other instance members in
		// static method
	}
	// Can we access x and y variable with in any static method?
}
