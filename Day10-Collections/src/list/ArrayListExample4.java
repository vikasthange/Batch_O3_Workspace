package list;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class ArrayListExample4 {

	public static void main(String[] args) {
		
		java.util.List<String> names = new ArrayList<String>();
		
		names.add("dsjsdjk");
		names.add("ajkjkh");
		names.add("fdh");
		names.add("rty");
		names.add("vgsecg");
		names.add("dg");
		names.add("vtfghs");
		names.add("bxm");
		names.add("eruiti");
		names.add("laskm");
		names.add("nbmv");
		names.sort(new DescComp());
		
		System.out.println("After sorting: "+ names);
	}
}

class DescComp implements Comparator<String> {

	@Override
	public int compare(String o1, String o2) {
		int diff = o1.compareTo(o2);
		System.out.println("Diff:"+ diff);
		if (diff <0) {
			return +1;
		}
		if (diff >0) {
			return -1;
		}

		return 0;
	}

}
