package list;

import java.util.ArrayList;

public class ArrayListExample2 {
	
	public static void main(String[] args) {
		ArrayList<Integer> list = new ArrayList<Integer>();
		
		System.out.println("is list empty initially: "+ list.isEmpty());
		list.add(2);  // 0
		list.add(78); // 1
		list.add(43); // 2
		list.add(89); // 3
		System.out.println("is list empty after adding values: "+ list.isEmpty());
		
		System.out.println("Size: "+ list.size());
		
		System.out.println("Lets add or more");
		list.add(100);
		System.out.println("After adding: "+ list.size());
		list.remove(3);
		System.out.println("after removing value at index 3:"+ list);
		boolean isRemoved = list.remove(Integer.valueOf(100));
		System.out.println("is value removed: "+ isRemoved);
		System.out.println("after removing value 100:"+ list);
		
		for (int i = 0; i < list.size(); i++) {
			System.out.println( list.get(i));
		}
		
		
		// conver array list to Array
		Integer[] arr = list.toArray(new Integer[1]);
		for (int i = 0; i < arr.length; i++) {
			System.out.println("Array value: "+ arr[i]);
		}
		list.add(1, 9999);
		System.out.println("Added 9999 at index 1: "+ list);
	}
}
