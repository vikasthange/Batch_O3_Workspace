package list;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ArraysListExample3 {

	public static void main(String[] args) {
		
		List<String> names = new ArrayList<String> ();
		names.add("Vikas");
		names.add("Raj Chavan");
		names.add(0,"Prajakta");
		
		String[] n = names.toArray(new String[0]);
		for (int i = 0; i < n.length; i++) {
			System.out.println("INDEX: "+i+", Value: "+n[i]);
		}
		
		for (int i = 0; i < names.size(); i++) {
			System.out.println(names.get(i));
		}
		
		System.out.println("After sorting: "+ names);
	}
}
