package list;

import java.util.ArrayList;

public class ArrayListExample1 {

	public static void main(String[] args) {
	
		ArrayList list = new ArrayList();
		list.add(2);
		list.add(78);
		list.add(43);
		list.add(89);
		list.add("Vikas Thange");
		list.add(3784387.0f);
		list.add(4378578.0);
		System.out.println("List is :"+ list.toString());
	}
}
