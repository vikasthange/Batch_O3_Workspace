
 abstract public class Shape {

	void drawShape() {
		System.out.println("drawing shape");
	}

	 abstract void calcArea(); // must  be overridden in all derived classes

	final void getPerimeter() {
		System.out.println("Capculating perimeter of shape");
	}
}
