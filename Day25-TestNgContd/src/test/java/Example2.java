import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

public class Example2 {

	WebDriver driver;
	
	@Test
	public void testSearch(){
		System.setProperty("webdriver.chrome.driver", "e:\\drivers\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("http://demo.testfire.net");
		driver.findElement(By.id("txtSearch")).sendKeys("vikas"+Keys.ENTER);
		
		String pageTitle = driver.findElement(By.tagName("h1")).getText();
		Assert.assertEquals(pageTitle	, "Search Results","Page title failed");
		String textSearched = driver.findElement(By.cssSelector("[id*='Content_Main_lblSearch']")).getText();
		Assert.assertEquals(textSearched, "vikas","search text did not match.");
		driver.quit();
	}
	
}
