import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

public class Example1 {

	
	
	@Test
	public void testLogin(){
		System.setProperty("webdriver.chrome.driver", "e:\\drivers\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("http://demo.testfire.net");
		driver.findElement(By.linkText("ONLINE BANKING LOGIN")).click();
		driver.findElement(By.id("uid")).sendKeys("jsmith");
		driver.findElement(By.id("passw")).sendKeys("demo1234");
		driver.findElement(By.id("passw")).submit();
		//driver.findElement(By.name("btnSubmit")).click();
		
		String welcomeMessage = driver.findElement(By.tagName("h1")).getText();
		Assert.assertEquals(welcomeMessage, "Hello John Smith","Greeting message failed");
		driver.quit();
	}
	@Test
	public void testAdminLogin(){
		System.setProperty("webdriver.chrome.driver", "e:\\drivers\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("http://demo.testfire.net");
		driver.findElement(By.linkText("ONLINE BANKING LOGIN")).click();
		driver.findElement(By.id("uid")).sendKeys("admin");
		driver.findElement(By.id("passw")).sendKeys("admin");
		driver.findElement(By.id("passw")).submit();
		//driver.findElement(By.name("btnSubmit")).click();
		
		String welcomeMessage = driver.findElement(By.tagName("h1")).getText();
		Assert.assertEquals(welcomeMessage, "Hello Admin User","Greeting message failed");
		driver.quit();
	}
}
