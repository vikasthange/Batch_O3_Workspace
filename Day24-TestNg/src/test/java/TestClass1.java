import org.testng.Assert;
import org.testng.annotations.Test;

public class TestClass1 {
	// annotation
	@Test(enabled = true, description = "this is a sample text case")
	public void testCase1() {

	}

	@Test(invocationCount=10)
	public void testCase2() {

		// if test is not NPE throwing exception then it will fail
	}

	@Test
	public void testCase3() {

		try {
			int no = 3 / 0;
		} catch (ArithmeticException e) {
			Assert.fail("Invalid operation, we can not device by 0", e);
		}
	}
}
