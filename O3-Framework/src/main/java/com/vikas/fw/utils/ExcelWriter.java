package com.vikas.fw.utils;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelWriter {

	public static void saveResult(String filePath,String sheetname,List<String> status, int colNo) {
		Workbook wb =null;
		try{
			wb = new XSSFWorkbook(filePath);
			Sheet sheet = wb.getSheet(sheetname);
			
			for (int i = 1; i <= status.size(); i++) {
				Row row = sheet.getRow(i); // 0th row is always heading
				if(row == null){
					row = sheet.createRow(i);
				}
				// Cell
				Cell cell = row.getCell(colNo);
				if(cell==null){
					cell = row.createCell(colNo);
				}
				
				cell.setCellValue(status.get(i-1));
			}
		}
		catch(Exception e){
			throw new RuntimeException("Failed to save result in excel file",e);
		}
		finally{
			if(wb!=null){
				// save the workbook
				try {
					OutputStream os = new FileOutputStream(filePath+"_op.xlsx");
					wb.write(os);
					wb.close();
					
					// if you want delete input file
					Files.delete(Paths.get(filePath));
					// and save _op.xlsx file as that of input file
					Files.move(Paths.get(filePath+"_op.xlsx"),Paths.get(filePath));
					
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		}
		
	}

	
}
