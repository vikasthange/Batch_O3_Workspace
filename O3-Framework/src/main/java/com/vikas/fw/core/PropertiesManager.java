package com.vikas.fw.core;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;

import javax.management.RuntimeErrorException;

import com.google.common.io.Resources;

public class PropertiesManager {

	static Properties props = null;

	static {
		props = new Properties();
		try {
			props.load(Resources.getResource("config.properties").openStream());

		} catch (IOException e) {
			throw new RuntimeException("Configuration file can not be loaded", e);
		}
	}

	public static String getChromeDriverPath() {
		return props.getProperty(PropNames.CHROME_DRIVER_PATH.name());
	}

	public static String getGeckoDriverPath() {
		return props.getProperty(PropNames.GECKO_DRIVER_PATH.name());
	}

	public static String getBrowserName() {
		return props.getProperty(PropNames.BROWSER_NAME.name());
	}

	public static int getImplicitTimeout() {
		return Integer.parseInt(props.getProperty(PropNames.IMPLICIT_TIMEOUT.name()));
	}

	public static final String ALTORO_MUTUAL_USERS_DATA = "src/main/resources/accounts_input_data.xlsx";

	enum PropNames {
		CHROME_DRIVER_PATH, GECKO_DRIVER_PATH, BROWSER_NAME, IMPLICIT_TIMEOUT, USE_GRID, GRID_HUB_URL;

	}

	public static boolean isGridSelected() {
		try {
			String value = props.getProperty(PropNames.USE_GRID.name());
			return Boolean.parseBoolean(value);
		} catch (Exception e) {
			return false;
		}
	}

	public static URL getHubUrl() {
		try{
			return new URL( props.getProperty(PropNames.GRID_HUB_URL.name()));
		}
		catch(MalformedURLException e){
			throw new RuntimeException("Invalid hub url, please correct hub rual and run again",e);
		}

	}
}
