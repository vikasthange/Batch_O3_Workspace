package com.vikas.fw.core;

import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

public class BrowserFactory {

	public static WebDriver getInstance() {
		WebDriver driver = null;

		if (PropertiesManager.isGridSelected()) {
			// On Selenium grid
			URL hubUrl = PropertiesManager.getHubUrl();
			DesiredCapabilities caps = new DesiredCapabilities();
			caps.setCapability(CapabilityType.BROWSER_NAME, PropertiesManager.getBrowserName());

			driver = new RemoteWebDriver(hubUrl, caps);

		} else {
			// Local computer
			switch (PropertiesManager.getBrowserName().toUpperCase()) {
			case "CHROME":
				System.setProperty("webdriver.chrome.driver", PropertiesManager.getChromeDriverPath());
				driver = new ChromeDriver();
				break;
			case "FIREFOX":
				System.setProperty("webdriver.gecko.driver", PropertiesManager.getGeckoDriverPath());
				driver = new FirefoxDriver();
				break;

			default:
				throw new RuntimeException("Invalid browser name: " + PropertiesManager.getBrowserName());

			}
		}

		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(PropertiesManager.getImplicitTimeout(), TimeUnit.SECONDS);
		return driver;

	}
}
