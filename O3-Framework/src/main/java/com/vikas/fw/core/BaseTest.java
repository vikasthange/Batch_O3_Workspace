package com.vikas.fw.core;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public abstract class BaseTest {

	//private WebDriver driver;
	//Map<String, WebDriver> driverMap = new HashMap<String, WebDriver>();
	ThreadLocal<WebDriver> drivers = new ThreadLocal<WebDriver>();
	
	@BeforeMethod
	public void beforeMethod() {
		WebDriver driver  = BrowserFactory.getInstance();
		//driverMap.put( Thread.currentThread().getName(), driver);
		drivers.set(driver);
		// current thread name (dynamic, internal) is key and driver is value
	}

	@AfterMethod
	public void cleanUp() {
		if (driver() != null) {
			driver().quit();
		}
	}

	protected WebDriver driver() {
		// return driver object of currently executing thread
		//return driverMap.get(Thread.currentThread().getName());
		return drivers.get();
	}
}
