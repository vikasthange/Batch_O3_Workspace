package com.vikas.learning.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.vikas.fw.core.BasePage;

public class GoogleSearchResultPage extends BasePage{
	
	public GoogleSearchResultPage(WebDriver driver) {
		super(driver);
	}

	@FindBy(id="resultStats")
	WebElement divResultStats;
	
	@FindBy(name="q")
	WebElement txtSearchBox;
	
	public String getSearchedText(){
		return txtSearchBox.getAttribute("value");
	}
	
}
