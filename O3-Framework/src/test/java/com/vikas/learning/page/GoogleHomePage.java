package com.vikas.learning.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.vikas.fw.core.BasePage;
import com.vikas.fw.utils.SyncUtil;

public class GoogleHomePage extends BasePage{

	
	public GoogleHomePage(WebDriver driver) {
		super(driver);
	}

	// Data Member (Fields)
	@FindBy(name="q")
	WebElement txtQueryBox;
	
	@FindBy(name="btnK")
	WebElement btnGoogleSearch;
	// Methods
	
	public GoogleSearchResultPage performSearch(String textToSearch){
		txtQueryBox.sendKeys(textToSearch);
		txtQueryBox.submit();
		SyncUtil.wait(3);
		//btnGoogleSearch.click();
		return PageFactory.initElements(driver(), GoogleSearchResultPage.class);
	}
	
	
}
