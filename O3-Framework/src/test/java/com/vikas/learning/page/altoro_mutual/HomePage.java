package com.vikas.learning.page.altoro_mutual;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.vikas.fw.core.BasePage;

public class HomePage extends BasePage{

	public HomePage(WebDriver driver) {
		super(driver);
	}
	
	@FindBy(linkText="ONLINE BANKING LOGIN")
	private WebElement linkLoginPage;
	
	public LoginPage openLoginPage(){
		linkLoginPage.click();
		return PageFactory.initElements(driver(), LoginPage.class);
	}
}
