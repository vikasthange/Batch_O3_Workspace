package com.vikas.learning.page.altoro_mutual;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.vikas.fw.core.BasePage;

public class UserHomePage extends BasePage{

	public UserHomePage(WebDriver driver) {
		super(driver);
	}
	
	@FindBy(id="listAccounts")
	private WebElement listAccounts;
	
	@FindBy(id="btnGetAccount")
	private WebElement btnGo;
	
	@FindBy(tagName="h1")
	private WebElement txtWelComeMessage;
	
	public String getWelcomeMessage(){
		return txtWelComeMessage.getText();
	}
	
	public void selectAccount(String accountText){
		Select select = new Select(listAccounts);
		select.selectByVisibleText(accountText);
	}
	public void clickGoButton(){
		btnGo.click();
	}
	public AccountHistoryPage goToAccount(String accountText){
		selectAccount(accountText);
		clickGoButton();
		// return next page object Account statement
		return PageFactory.initElements(driver(), AccountHistoryPage.class);
	}
}
