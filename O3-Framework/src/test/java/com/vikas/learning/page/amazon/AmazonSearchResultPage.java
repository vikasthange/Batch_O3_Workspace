package com.vikas.learning.page.amazon;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.vikas.fw.core.BasePage;

public class AmazonSearchResultPage extends BasePage{

	public AmazonSearchResultPage(WebDriver driver) {
		super(driver);
	}
	
	@FindBy(id="autoscoping-backlink")
	WebElement divShowingResultText;
	
	
	public String getSearchedResultText(){
		return divShowingResultText.getText();
	}

	
}
