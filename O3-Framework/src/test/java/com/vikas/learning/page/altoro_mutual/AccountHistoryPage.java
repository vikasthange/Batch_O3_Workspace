package com.vikas.learning.page.altoro_mutual;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.vikas.fw.core.BasePage;

public class AccountHistoryPage extends BasePage{

	public AccountHistoryPage(WebDriver driver) {
		super(driver);
	}
	
	@FindBy(css="[id*='Main_accountid']")
	private WebElement txtCurrentAccountNumber;
	
	@FindBy(xpath="//*[contains(text(),'Available balance')]/following-sibling::td/span")
	private WebElement txtCurrentBalance;
	
	public String getAccountNumber(){
		return txtCurrentAccountNumber.getText();
	}
	
	public String getCurrentBalance(){
		return txtCurrentBalance.getText();
	}
	
}
