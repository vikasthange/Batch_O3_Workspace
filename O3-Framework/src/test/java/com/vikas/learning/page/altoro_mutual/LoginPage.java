package com.vikas.learning.page.altoro_mutual;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.vikas.fw.core.BasePage;

public class LoginPage extends BasePage{

	public LoginPage(WebDriver driver) {
		super(driver);
	}

	@FindBy(id="uid")
	private WebElement txtUserId;
	
	@FindBy(id="passw")
	private WebElement txtPassword;
	
	@FindBy(name="btnSubmit")
	private WebElement btnLogin;
	
	public UserHomePage performLogin(String uname, String password){
		txtUserId.sendKeys(uname);
		txtPassword.sendKeys(password);
		btnLogin.click();
		return PageFactory.initElements(driver(), UserHomePage.class);
	}
	
}
