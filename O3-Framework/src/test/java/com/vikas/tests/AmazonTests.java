package com.vikas.tests;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.vikas.fw.core.BaseTest;
import com.vikas.learning.page.amazon.AmazonHomePage;
import com.vikas.learning.page.amazon.AmazonSearchResultPage;

public class AmazonTests extends BaseTest {

	@BeforeMethod
	public void openUrl() {
		driver().get("http://amazon.in");
	}

	@Test
	public void testProductSearch() {

		AmazonHomePage homePage = PageFactory.initElements(driver(), AmazonHomePage.class);
		AmazonSearchResultPage resultPage = homePage.searchProduct("pixel");
		String actualText = resultPage.getSearchedResultText();
		Assert.assertEquals(actualText, "Showing results in Electronics.Show instead results in All Categories.",
				"search result text failed.");
	}

	// example of parameterized test case - passing value from suite xml file
	@Parameters(value = { "product_name", "param2" })
	@Test
	public void testProductSearch2(String product, String param2) {

		AmazonHomePage homePage = PageFactory.initElements(driver(), AmazonHomePage.class);
		AmazonSearchResultPage resultPage = homePage.searchProduct(product);
		String actualText = resultPage.getSearchedResultText();
		Assert.assertEquals(actualText, "Showing results in Electronics.Show instead results in All Categories.",
				"search result text failed.");
	}

	// example of parameterized test case - passing value from suite xml file
	@Parameters(value = { "product_name" })
	@Test
	public void testProductSearch3(@Optional("pixel") String product) {

		AmazonHomePage homePage = PageFactory.initElements(driver(), AmazonHomePage.class);
		AmazonSearchResultPage resultPage = homePage.searchProduct(product);
		String actualText = resultPage.getSearchedResultText();
		Assert.assertEquals(actualText, "Showing results in Electronics.Show instead results in All Categories.",
				"search result text failed.");
	}

	@Test(dataProvider = "getSearchData")
	public void testProductSearch4(String product) {

		AmazonHomePage homePage = PageFactory.initElements(driver(), AmazonHomePage.class);
		AmazonSearchResultPage resultPage = homePage.searchProduct(product);
		String actualText = resultPage.getSearchedResultText();
		Assert.assertEquals(actualText, "Showing results in Electronics.Show instead results in All Categories.",
				"search result text failed.");
	}

	@DataProvider(parallel = true)
	public Object[][] getSearchData() {

		Object[][] data = new Object[][] { 
			{ "Pixel" }, 
			{ "iPhone" }, 
			{ "Samsung" } 
		};

		return data;
	}

}
