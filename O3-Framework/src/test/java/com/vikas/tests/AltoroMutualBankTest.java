package com.vikas.tests;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import org.uncommons.reportng.HTMLReporter;
import org.uncommons.reportng.JUnitXMLReporter;

import com.vikas.dp.AltoroMutualDataProviders;
import com.vikas.fw.core.BaseTest;
import com.vikas.fw.core.PropertiesManager;
import com.vikas.fw.utils.ExcelWriter;
import com.vikas.learning.page.altoro_mutual.AccountHistoryPage;
import com.vikas.learning.page.altoro_mutual.HomePage;
import com.vikas.learning.page.altoro_mutual.LoginPage;
import com.vikas.learning.page.altoro_mutual.UserHomePage;
@Listeners(value={HTMLReporter.class,JUnitXMLReporter.class})
public class AltoroMutualBankTest extends BaseTest{
	List<String> status = new ArrayList<String>();
	List<String> failMessage  = new ArrayList<String>();
	@BeforeMethod
	public void openAltoroMutualPage(){
		driver().get("http://demo.testfire.net");
	}
	@Test(dataProvider=AltoroMutualDataProviders.DP_USERNAME_PASS_ACC_EXP_BAL, dataProviderClass=AltoroMutualDataProviders.class)
	public void testAccountBalance(String username,String password, String account, String expectedBalance){
		HomePage homePage = PageFactory.initElements(driver(), HomePage.class);
		LoginPage loginPage = homePage.openLoginPage();
		UserHomePage userHome = loginPage.performLogin(username, password);
		Assert.assertEquals(userHome.getWelcomeMessage(), "Hello John Smith","Welcome text failed.");
		AccountHistoryPage accPage = userHome.goToAccount(account);
		Assert.assertTrue(account.contains(accPage.getAccountNumber()),account + " was not containing "+accPage.getAccountNumber() );
		Assert.assertEquals(accPage.getCurrentBalance(), expectedBalance,"Account balance did not match.");
	}
	@Test(dataProvider=AltoroMutualDataProviders.DP_MAP_USERNAME_PASS_ACC_EXP_BAL, dataProviderClass=AltoroMutualDataProviders.class)
	public void testAccountBalance(Map<String,String> params){
		HomePage homePage = PageFactory.initElements(driver(), HomePage.class);
		LoginPage loginPage = homePage.openLoginPage();
		UserHomePage userHome = loginPage.performLogin(params.get("username"), params.get("password"));
		Assert.assertEquals(userHome.getWelcomeMessage(), "Hello John Smith","Welcome text failed.");
		AccountHistoryPage accPage = userHome.goToAccount(params.get("account"));
		Assert.assertTrue(params.get("account").contains(accPage.getAccountNumber()),params.get("account") + " was not containing "+accPage.getAccountNumber() );
		Assert.assertEquals(accPage.getCurrentBalance(), params.get("expBalance"),"Account balance did not match.");
	}
	
	@Test(dataProvider=AltoroMutualDataProviders.DP_MAP_USERNAME_PASS_ACC_EXP_BAL_FROM_EXCEL, dataProviderClass=AltoroMutualDataProviders.class)
	public void testAccountBalanceFromExcelData(Map<String,String> params){
		HomePage homePage = PageFactory.initElements(driver(), HomePage.class);
		LoginPage loginPage = homePage.openLoginPage();
		UserHomePage userHome = loginPage.performLogin(params.get("username"), params.get("password"));
		Assert.assertEquals(userHome.getWelcomeMessage(), "Hello John Smith","Welcome text failed.");
		AccountHistoryPage accPage = userHome.goToAccount(params.get("account"));
		
		Assert.assertTrue(params.get("account").contains(accPage.getAccountNumber()),params.get("account") + " was not containing "+accPage.getAccountNumber() );
		
		double actualBalance = Double.parseDouble(accPage.getCurrentBalance());
		double epectedBalance = Double.parseDouble(params.get("expBalance"));
		
		Assert.assertEquals(actualBalance, epectedBalance,"Account balance did not match.");
	}
	@AfterMethod
	public void collectReport(ITestResult result){
		String txtStatus = "pass";
		String failureMessage="";
		if(result.getStatus() == ITestResult.FAILURE){
			txtStatus = "fail";
			failureMessage = result.getThrowable().getClass()+" - "+ result.getThrowable().getMessage();
		}
		else if(result.getStatus() == ITestResult.SKIP){
			txtStatus = "skip";
			failureMessage = result.getThrowable().getClass()+" - "+ result.getThrowable().getMessage();
		}
		status.add(txtStatus);
		failMessage.add(failureMessage);
		
	}
	
	@AfterSuite
	public void saveResult(){
		ExcelWriter.saveResult(PropertiesManager.ALTORO_MUTUAL_USERS_DATA,"Sheet1",status,4);
		ExcelWriter.saveResult(PropertiesManager.ALTORO_MUTUAL_USERS_DATA,"Sheet1",failMessage,5);
	}
	
}
