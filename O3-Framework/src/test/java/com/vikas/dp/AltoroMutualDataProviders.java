package com.vikas.dp;

import java.util.HashMap;

import org.testng.annotations.DataProvider;

import com.vikas.fw.core.PropertiesManager;
import com.vikas.fw.utils.ExcelReader;

public class AltoroMutualDataProviders {

	public static final String DP_USERNAME_PASS_ACC_EXP_BAL="dpUsernamePasswordAccExpectedBalance";
	public static final String DP_MAP_USERNAME_PASS_ACC_EXP_BAL = "dpMAPUsernamePasswordAccExpectedBalance";
	public static final String DP_MAP_USERNAME_PASS_ACC_EXP_BAL_FROM_EXCEL="DP_MAP_USERNAME_PASS_ACC_EXP_BAL_FROM_EXCEL";
	@DataProvider(name=DP_USERNAME_PASS_ACC_EXP_BAL)
	public static Object[][] getAccountBalanceData(){
		Object[][] data = new Object[][]{
			{"jsmith","demo1234","1001160141 Savings","3000"},
			{"jsmith","demo1234","1001160140 Checking","-788"},
		};
		return data;
	}
	@DataProvider(name=DP_MAP_USERNAME_PASS_ACC_EXP_BAL)
	public static Object[][] getAccountBalanceDataAsMap(){
		HashMap<String, String> map1 = new HashMap<String,String>();
		map1.put("username", "jsmith");
		map1.put("password", "demo1234");
		map1.put("account", "1001160141 Savings");
		map1.put("expBalance", "3000");
		
		HashMap<String, String> map2 = new HashMap<String,String>();
		map2.put("username", "jsmith");
		map2.put("password", "demo1234");
		map2.put("account", "1001160140 Checking");
		map2.put("expBalance", "-788");
			
		
		return new Object[][]{{map1},{map2}};
	}
	@DataProvider(name=DP_MAP_USERNAME_PASS_ACC_EXP_BAL_FROM_EXCEL, parallel=true)
	public static Object[][] getAccountBalanceDataAsMapFromExcel(){
		// : Write script to read excel file (Apache POI), load data in 2D object array and then return
		
		Object[][] data = ExcelReader.getData(PropertiesManager.ALTORO_MUTUAL_USERS_DATA, "Sheet1");
		return data;
		
	}
}
