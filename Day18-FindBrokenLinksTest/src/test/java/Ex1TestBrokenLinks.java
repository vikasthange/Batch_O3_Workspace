import java.io.FileWriter;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class Ex1TestBrokenLinks {

	@Test
	public void testCase1() throws IOException {
		System.setProperty("webdriver.chrome.driver", "e:\\drivers\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("http://automationpractice.com");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		List<WebElement> allHyperlinks = driver.findElements(By.tagName("a"));

		Reporter.log("Total <a> tags : " + allHyperlinks.size(), true);
		FileWriter fw= new FileWriter("urls.txt");
		for (int i = 0; i < allHyperlinks.size(); i++) {
			WebElement link = allHyperlinks.get(i);
			String strUrl = link.getAttribute("href");
			//Reporter.log("Text: " + link.getText(), true);
			//Reporter.log("Hyperlink: " + strUrl, true);
			fw.write(strUrl+"\n");
//			try {
//				URL url = new URL(strUrl);
//				HttpURLConnection connection = (HttpURLConnection) url.openConnection();
//				connection.setRequestMethod("GET");
//				connection.connect();
//
//				int code = connection.getResponseCode();
//				if(code > 400){
//					Reporter.log(code+" - "+ url,true);
//				}
//			} catch (IOException e) {
//				System.err.println(e.getClass().getName()+" : URL: " + strUrl);
//			}

		}
		fw.close();
		System.out.println("Urls saved in file");
	}
}
