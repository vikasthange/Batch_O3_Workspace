import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

public class FlipkartAutomation {

	@Test
	public void testSearch() {
		System.setProperty("webdriver.gecko.driver", "e:\\drivers\\geckodriver.exe");
		// launch brwser
		WebDriver driver = new FirefoxDriver();
		driver.get("http://flipkart.com");
		driver.findElement(By.className("_29YdH8")).click();
		driver.findElement(By.name("q")).sendKeys("iPhone" + Keys.ENTER);
		// driver.findElement(By.className("vh79eN")).click()
		waitForSeconds(5);
		boolean isiPhoneLogoDisplayed = isDisplayed(driver, By.cssSelector("[alt='Great Deals on iPhones !!']"));
		// expected is isiPhoneLogoDisplayed must be true
		Assert.assertTrue(isiPhoneLogoDisplayed,"iPhone logo was not displayed");
		WebElement searchResultTextElement = driver.findElement(By.tagName("h1"));
		//System.out.println("Text found: "+searchResultTextElement.getText());
		//driver.quit();
		Assert.assertEquals(searchResultTextElement.getText(), "Showing 1 � 24 of 4,183 results for \"iphone\"","Result count failed");
		
		boolean contains = searchResultTextElement.getText().contains("iPhone");
		Assert.assertTrue(contains);
		//Assert.assertFalse(contains);

	}
	private void waitForSeconds(int sec) {
		try {
			Thread.sleep(sec * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	private boolean isDisplayed(WebDriver driver, By locator) {

		try {
			WebElement ele = driver.findElement(locator);
			if (ele.isDisplayed()) {
				return true;
			} else {
				return false;
			}
		} catch (NoSuchElementException e) {
			return false;
		}
	}
}
