
public class Example11 {
	public static void main(String[] args) {
		
		int a=43;
		int b=38;
		int c=329;
		
//		int max = a>b?a:b;
//		max = c>max?c:max;
		
		int max = a>b? (a>c?a:c) : (b>c?b:c);
		
		System.out.println("Max is "+max);
	}
}
