
public class Example9 {

	public static void main(String[] args) {
		// with out Turnary operator
		
		int a = 10;
		int b = 20;
		int max;
		if(a>b){
			max = a;
		}
		else{
			max = b;
		}
		System.out.println("max is : "+max);
	}
}
