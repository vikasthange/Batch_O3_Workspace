
public class Example3 {

	public static void main(String[] args) {
		int intNo = 101; // 4 bytes
		
		float floatNo = intNo; // 4 bytes (implicit casting)
		
		System.out.println(floatNo);// will print as 101.0
		
	}
}
