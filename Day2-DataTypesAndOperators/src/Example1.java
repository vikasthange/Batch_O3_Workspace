
public class Example1 {

	public static void main(String[] args) {
		
		int no=123;  // 4 byte
		byte no2= 23;
		long no3 = 1018888888888888889l; // 8 bytes
//		no3=no; // allowed
//		no = no3; // not allowed
		
//		no = (int) no3; // type casting
//		System.out.println("no: "+no);
		
		no = no2;  // 4 byte = 1 bytes
		byte no4 = no2;
		
		
		
		float f1 = 23456.38f;
		int no5 = (int) f1;
		System.out.println(no5);
		
		
	}
}
