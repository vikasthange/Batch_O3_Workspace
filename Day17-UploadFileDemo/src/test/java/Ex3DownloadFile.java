import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class Ex3DownloadFile {

	@Test
	public void testFileDownload() throws IOException{
		System.setProperty("webdriver.chrome.driver","e:\\drivers\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://online2pdf.com/doc-to-pdf");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		driver.findElement(By.id("input_file0")).sendKeys("C:\\Users\\vikas\\Desktop\\input.docx");
		driver.findElement(By.className("convert_button")).click();
		
		WebElement linkElementToDownload = driver.findElement(By.linkText("Manual download"));
		String url= linkElementToDownload.getAttribute("href");
		System.out.println("Download link : "+ url);
		
		url=url.replace("https", "http");
		URL website = new URL(url);
		ReadableByteChannel rbc = Channels.newChannel(website.openStream());
		FileOutputStream fos = new FileOutputStream("e:\\input.pdf");
		fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
		System.out.println("File download sucessfully");
	}
}
