import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class Ex2UploadFileWithNonStandardFileUpload {

	@Test
	public void testCase1(){
		System.setProperty("webdriver.chrome.driver","e:\\drivers\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://smallpdf.com/word-to-pdf");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		driver.findElement(By.className("omnibox-link")).click(); 
		// will open upload file dialog - cant automate using selenium
		try {
			Thread.sleep(2000);
			// Robot Class - Adv Java AWT - used to simulate keyboard and mouse actions 
			Robot robot = new Robot();
			RobotUtils.typeKeys("C:\\Users\\vikas\\Desktop\\input.docx", robot);
			robot.keyPress(KeyEvent.VK_ENTER);
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
}
