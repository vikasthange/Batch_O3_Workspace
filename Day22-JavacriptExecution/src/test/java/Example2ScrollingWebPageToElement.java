import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class Example2ScrollingWebPageToElement {

	@Test
	public void testCase1() throws InterruptedException{
		System.setProperty("webdriver.chrome.driver","e:\\drivers\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("http://flipkart.com");
		
		Thread.sleep(5000);
		JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
		//jsExecutor.executeScript("document.getElementsByTagName('footer')[0].scrollIntoView()");
		
		WebElement element = driver.findElement(By.cssSelector("[data-reactid='1204']"));
		jsExecutor.executeScript("arguments[0].scrollIntoView()", element);
		
		Object value = jsExecutor.executeScript("return window.scrollY");
		
		System.out.println("Scroll Y value from webpage is : "+ value);
		
		
		
	}
}
