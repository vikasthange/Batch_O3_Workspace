import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class Example3 {

	@Test
	public void testCase1() throws InterruptedException{
		System.setProperty("webdriver.chrome.driver","e:\\drivers\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("http://google.com");
		
		Thread.sleep(5000);
		JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
		//jsExecutor.executeScript("document.getElementsByTagName('footer')[0].scrollIntoView()");
		
		jsExecutor.executeScript("document.getElementsByName('q')[0].value='Vikas Thange'");
		
		
//		Object value = jsExecutor.executeScript("return document.getElementsByName('q')[0].value");
//		String str = (String) value;
//		System.out.println("Value form text box:"+ str);
		
		String txt =driver.findElement(By.name("q")).getAttribute("value");
		System.out.println("Value from text box using WebDriver : "+ txt);
		
	}
}
