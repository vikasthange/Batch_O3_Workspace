import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class Example1ShowAlert {

	@Test
	public void testCase1(){
		System.setProperty("webdriver.chrome.driver","e:\\drivers\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("http://google.com");
		
		String js1 ="arguments[0].value=\"Vikas Thange\"";
		//String js2 = "document.getElementsByName(\"btnK\")[0].click()";
		
		WebElement txtQuery = driver.findElement(By.name("q"));
		//.sendKeys("vikas thange");;
		
		JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
		jsExecutor.executeScript("arguments[0].value='Vikas Thange'",txtQuery);
		//jsExecutor.executeScript(js2);
	}
}
