import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ListExample {

	public static void main(String[] args) {
		
		
		// Converting List into Array
		
		// Covert arrays into list
		Integer arr[] = new Integer[]{1,2,3,4,5,6,7,8,9,0};
		
		List<Integer> list = Arrays.asList(arr);
		System.out.println(list.get(2));
		System.out.println(list.size());
	}
}
