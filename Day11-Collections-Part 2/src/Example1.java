import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * Class to demonstrate use of Set collection
 * @author vikas
 *
 */
public class Example1 {

	
	public static void main(String[] args) {
		
		Set<String> set = new HashSet<String>();
		set.add("Vikas");
		set.add("nayan");
		set.add("Raju");
		set.add("Swpna");
		set.add("farhan");
		boolean isAdded = set.add("Vikas"); // no error
		System.out.println("Is vikas added again? "+ isAdded);
		System.out.println(set);
		
		// For each loop
//		for (String val : set) {
//			System.out.println("-> "+ val);
//		}
		// using iterator
		Iterator<String> itr = set.iterator();
		while(itr.hasNext()){
			System.out.println("Value: "+ itr.next() );
		}
		
		List<String> list = new ArrayList<String>();
		list.addAll(set);
		System.out.println("List is: "+ list);
		
		
		
	}
}
