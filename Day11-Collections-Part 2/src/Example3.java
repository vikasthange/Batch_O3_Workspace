import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * Class to demonstrate use of  tree set collection
 * @author vikas
 *
 */
public class Example3 {

	
	public static void main(String[] args) {
		
		Set<String> set = new TreeSet<String>();
		set.add("Vikas");
		set.add("Nayan");
		set.add("Raju");
		set.add("Swpna");
		set.add("Farhan");
		boolean isAdded = set.add("Vikas"); // no error
		System.out.println("Is vikas added again? "+ isAdded);
		System.out.println(set);
		
		// For each loop
//		for (String val : set) {
//			System.out.println("-> "+ val);
//		}
		// using iterator
		Iterator<String> itr = set.iterator();
		while(itr.hasNext()){
			System.out.println("Value: "+ itr.next() );
		}
		
		List<String> list = new ArrayList<String>();
		list.addAll(set);
		System.out.println("List is: "+ list);
		
		
		
	}
}
