import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class AutomationPractice {

	public static void main(String[] args) {
		// Step 1: Load the driver
		System.setProperty("webdriver.chrome.driver", "e:\\drivers\\chromedriver.exe");
		// launch brwser
		WebDriver driver = new ChromeDriver();
		driver.get("http://automationpractice.com");
		driver.findElement(By.className("login")).click();
		driver.findElement(By.id("email_create")).sendKeys("vikasthange@gmail.com");
		driver.findElement(By.cssSelector("#SubmitCreate > span")).click();
		WebElement radioMr = driver.findElement(By.id("id_gender1"));
		System.out.println("is Mr is selected before click: " +radioMr.isSelected() );
		driver.findElement(By.id("id_gender1")).click();
		System.out.println("is Mr is selected after click: " +radioMr.isSelected() );
	}
}
