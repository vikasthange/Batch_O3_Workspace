import java.io.File;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.annotations.Test;

import com.google.common.io.Files;

public class Example1 {

	@Test
	public void test1(){
		
		System.setProperty("webdriver.chrome.driver", "e:\\drivers\\chromedriver.exe");
		
		EventFiringWebDriver driver = new EventFiringWebDriver(new ChromeDriver());
		MyWebDriverListener listener = new MyWebDriverListener();
		driver.register(listener);
		
		
		driver.get("http://google.com");
		
		driver.findElement(By.name("q")).click();
		driver.findElement(By.name("q")).sendKeys("Vikas Thange"+Keys.ENTER);
		
		
	}
}
