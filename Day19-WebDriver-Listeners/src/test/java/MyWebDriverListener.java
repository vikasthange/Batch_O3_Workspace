import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.AbstractWebDriverEventListener;
import org.testng.Reporter;

public class MyWebDriverListener extends AbstractWebDriverEventListener{

	static int counter=1;
	
	@Override
	public void beforeNavigateTo(String url, WebDriver driver) {
		Reporter.log("Before opening url:"+url+"...",true);
	}

	@Override
	public void afterNavigateTo(String url, WebDriver driver) {
		SnapsUtil.captureSnap(driver, "snaps\\"+ counter++ +".png");
		Reporter.log("url opened:"+url+" in browser",true);
		Reporter.log("Page Title: "+driver.getTitle(),true);
	}
	
	@Override
	public void beforeClickOn(WebElement element, WebDriver driver) {
		Reporter.log("Clicking on element ..."+ element,true);
	}
	
	@Override
	public void afterClickOn(WebElement element, WebDriver driver) {
		SnapsUtil.captureSnap(driver, "snaps\\"+ counter++ +".png");
		Reporter.log("Element clicked: "+element,true);
	}
	
	@Override
	public void beforeChangeValueOf(WebElement element, WebDriver driver, CharSequence[] keysToSend) {
		Reporter.log("Typing text: "+keysToSend);
	}
	@Override
	public void afterChangeValueOf(WebElement element, WebDriver driver, CharSequence[] keysToSend) {
		SnapsUtil.captureSnap(driver, "snaps\\"+ counter++ +".png");
		Reporter.log("Value set to text box");
	}
}
