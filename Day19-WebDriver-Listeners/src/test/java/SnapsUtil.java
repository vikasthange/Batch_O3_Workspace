import java.io.File;
import java.io.IOException;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import com.google.common.io.Files;

public class SnapsUtil {

	public static void captureSnap(WebDriver driver, String fileName){
		TakesScreenshot takeSnap = (TakesScreenshot) driver;
		File snap= takeSnap.getScreenshotAs(OutputType.FILE);
		System.out.println("Snap Path: "+ snap.getAbsolutePath());
		
		File dest = new File(fileName); // relative path
		try {
			Files.move(snap, dest);
			System.out.println("File moved to: "+ dest.getAbsolutePath()); // c:\
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
