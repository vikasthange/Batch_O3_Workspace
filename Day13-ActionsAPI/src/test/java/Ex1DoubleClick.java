import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.Test;

public class Ex1DoubleClick {
	
	@Test
	public void testDoubleClick(){
		System.setProperty("webdriver.chrome.driver", "e:\\drivers\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://api.jquery.com/dblclick/");
		
		WebElement frameElement = driver.findElement(By.tagName("iframe"));
		driver.switchTo().frame(frameElement);
		
		WebElement elem = driver.findElement(By.cssSelector("body > div"));
		String color = elem.getCssValue("background-color");
		System.out.println("Before double click: "+ color);
		Assert.assertEquals(color, "rgba(0, 0, 255, 1)","Blue color not found before click");
		Actions actions = new Actions(driver);
		actions.doubleClick(elem).perform();
		
		elem = driver.findElement(By.cssSelector("body > div"));
		color = elem.getCssValue("background-color");
		System.out.println("After double click: "+ color);
		Assert.assertEquals(color, "rgba(255, 255, 0, 1)","Yellow color not found after click");
		
		
	
		
		
	}
}
